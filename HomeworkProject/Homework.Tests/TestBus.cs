using Homework.BLL.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Homework.Tests
{
    [TestClass]
    public class TestBus
    {
        [TestMethod]
        public void TestBusDiscountNone()
        {
            Passanger passanger = new Passanger(1)
            {
                isDiscount = false,
                baseTicketPrice = 25
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport bus = new Bus(2, "Boris", passangers);
            Assert.AreEqual(25, bus.getFinalCost());
        }

        [TestMethod]
        public void TestBusDiscount()
        {
            Passanger passanger = new Passanger(3)
            {
                isDiscount = true,
                baseTicketPrice = 25
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport bus = new Bus(4, "Booris", passangers);
            Assert.AreEqual(22.5, bus.getFinalCost());
        }

        [TestMethod]
        public void TestBusChangePrice()
        {
            Passanger passanger = new Passanger(5)
            {
                isDiscount = true,
                baseTicketPrice = 50
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport bus = new Bus(6, "Booris", passangers);
            Assert.AreEqual(22.5, bus.getFinalCost());
        }
    }
}
