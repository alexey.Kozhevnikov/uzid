﻿using Homework.BLL.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Homework.Tests
{
    [TestClass]
    public class TestTaxi
    {
        [TestMethod]
        public void TestTaxiOneRide()
        {
            Passanger passanger = new Passanger(1)
            {
                rideDistance = 10000
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport taxi = new Taxi(2, "Boris", passangers);
            Assert.AreEqual(150, taxi.getFinalCost());
        }

        [TestMethod]
        public void TestTaxiFewRides()
        {
            List<Passanger> passangers = new List<Passanger>();
            Passanger passanger1 = new Passanger(3)
            {
                rideDistance = 10000
            };
            Passanger passanger2 = new Passanger(4)
            {
                rideDistance = 20000
            };
            passangers.Add(passanger1);
            passangers.Add(passanger2);
            Transport taxi = new Taxi(5, "Boris", passangers);
            Assert.AreEqual(400, taxi.getFinalCost());
        }
    }
}
