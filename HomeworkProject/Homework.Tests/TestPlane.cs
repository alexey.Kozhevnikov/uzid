﻿using Homework.BLL.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Homework.Tests
{
    [TestClass]
    public class TestPlane
    {
        [TestMethod]
        public void TestPlaneDiscountNone()
        {
            Passanger passanger = new Passanger(1)
            {
                isDiscount = false,
                baseTicketPrice = 10000
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport plane = new Plane(2, "Boris", passangers);
            Assert.AreEqual(10000, plane.getFinalCost());
        }

        [TestMethod]
        public void TestPlaneDiscount()
        {
            Passanger passanger = new Passanger(3)
            {
                isDiscount = true,
                baseTicketPrice = 10000
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport plane = new Plane(4, "Boris", passangers);
            Assert.AreEqual(8000, plane.getFinalCost());
        }
    }
}
