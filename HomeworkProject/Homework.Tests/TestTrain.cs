﻿using Homework.BLL.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Homework.Tests
{
    [TestClass]
    public class TestTrain
    {
        [TestMethod]
        public void TestTrainDiscountNoneChild()
        {
            Passanger passanger = new Passanger(1)
            {
                isDiscount = false,
                baseTicketPrice = 10000,
                age = 15
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport train = new Train(2, "Boris", passangers);
            Assert.AreEqual(5000, train.getFinalCost());
        }

        [TestMethod]
        public void TestTrainDiscountNoneAdult()
        {
            Passanger passanger = new Passanger(3)
            {
                isDiscount = false,
                baseTicketPrice = 10000,
                age = 30
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport train = new Train(4, "Booris", passangers);
            Assert.AreEqual(10000, train.getFinalCost());
        }

        [TestMethod]
        public void TestTrainDiscountChild()
        {
            Passanger passanger = new Passanger(5)
            {
                isDiscount = true,
                baseTicketPrice = 20000,
                age = 12
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport train = new Train(6, "Booris", passangers);
            Assert.AreEqual(9000, train.getFinalCost());
        }

        [TestMethod]
        public void TestTrainDiscountAdult()
        {
            Passanger passanger = new Passanger(7)
            {
                isDiscount = true,
                baseTicketPrice = 20000,
                age = 30
            };
            List<Passanger> passangers = new List<Passanger>
            {
                passanger
            };
            Transport train = new Train(8, "Booris", passangers);
            Assert.AreEqual(18000, train.getFinalCost());
        }
    }
}
