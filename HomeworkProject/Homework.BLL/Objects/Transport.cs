﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework.BLL.Objects
{
    public abstract class Transport
    {
        private int id;
        private string driverName;

        public Transport(int id, string name) {
            this.id = id;
            this.driverName = name;
        }

        public Transport() {
            this.id = 0;
            this.driverName = "Anonymous";

        }

        public abstract double getFinalCost();
        public int getId()
        {
            return id;
        }
    }
}
