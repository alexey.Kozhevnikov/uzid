﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework.BLL.Objects
{
    public class Train : Transport
    {
        private int passangersCount;
        private List<Passanger> passangers;

        public Train(int id, string name, List<Passanger> passangers) : base(id, name)
        {
            this.passangersCount = passangers.Count;
            this.passangers = passangers;
        }

        public override double getFinalCost()
        {
            double result = 0;
            for (int i = 0; i < passangersCount; i++)
            {
                Passanger person = passangers[i];
                double ticket = 0;
                if (checkChildDiscount(person)) {
                    ticket = person.baseTicketPrice * 0.5;
                } else
                {
                    ticket = person.baseTicketPrice;
                }

                if (person.isDiscount)
                {
                    result += ticket * 0.9;
                }
                else
                {
                    result += ticket;
                }
            }
            return result;
        }

        private bool checkChildDiscount(Passanger passanger) {
            if (passanger.age <= 16) return true;
            else return false;
        }
    }
}
