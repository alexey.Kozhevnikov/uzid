﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework.BLL.Objects
{
    public class Plane : Transport
    {
        private int passangersCount;
        private List<Passanger> passangers;

        public Plane(int id, string name, List<Passanger> passangers) : base(id, name)
        {
            this.passangersCount = passangers.Count;
            this.passangers = passangers;
        }

        public override double getFinalCost()
        {
            double result = 0;
            for (int i = 0; i < passangersCount; i++)
            {
                if (passangers[i].isDiscount)
                {
                    result += passangers[i].baseTicketPrice * 0.8;
                }
                else
                {
                    result += passangers[i].baseTicketPrice;
                }
            }
            return result;
        }
    }
}
