﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework.BLL.Objects
{
    public class Taxi : Transport
    {
        private int passangersCount;
        List<Passanger> passangers;
        private int arrivingCost = 50;
        private double rate = 10;

        public Taxi(int id, string name, List<Passanger> passangers) : base(id, name)
        {
            this.passangersCount = passangers.Count;
            this.passangers = passangers;
            passangers.ForEach(delegate (Passanger passanger)
            {
                passanger.baseTicketPrice = this.arrivingCost;
            });
        }

        public override double getFinalCost()
        {
            double result = 0;
            passangers.ForEach(delegate (Passanger passanger)
            {
                result += this.arrivingCost + ((passanger.rideDistance / 1000) * this.rate);
            });
            return result;
        }
    }
}
