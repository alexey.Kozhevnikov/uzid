﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework.BLL.Objects
{
    public class Passanger
    {
        private int id;
        public bool isDiscount { get; set; }
        public double baseTicketPrice { get; set; }
        public int age { get; set; }
        public double rideDistance { get; set; }


        /// <summary>
        /// Constructor for train and plane ticket
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isDiscount"></param>
        /// <param name="price"></param>
        /// <param name="age"></param>
        public Passanger(int id, bool isDiscount, double price, int age) {
            this.id = id;
            this.isDiscount = isDiscount;
            this.baseTicketPrice = price;
            this.age = age;
        }

        /// <summary>
        /// Constructor for bus ticket
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isDiscount"></param>
        /// <param name="age"></param>
        public Passanger(int id, bool isDiscount)
        {
            this.id = id;
            this.isDiscount = isDiscount;
        }

        /// <summary>
        /// Constructor for taxi ride
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isDiscount"></param>
        /// <param name="distance"></param>
        public Passanger(int id, bool isDiscount, double distance)
        {
            this.id = id;
            this.isDiscount = isDiscount;
            this.rideDistance = distance;
        }

        /// <summary>
        /// Universal constructor
        /// </summary>
        /// <param name="id"></param>
        /// <param name="isDiscount"></param>
        /// <param name="distance"></param>
        /// <param name="age"></param>
        /// <param name="price"></param>
        public Passanger(int id, bool isDiscount, double distance, int age, double price)
        {
            this.id = id;
            this.isDiscount = isDiscount;
            this.rideDistance = distance;
            this.age = age;
            this.baseTicketPrice = price;
        }

        public Passanger(int id)
        {
            this.id = id;
        }
    }
}
