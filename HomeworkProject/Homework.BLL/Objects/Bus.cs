﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework.BLL.Objects
{
    public class Bus : Transport
    {
        private int passangersCount;
        private List<Passanger> passangers;
        private double baseCost = 25;

        public Bus(int id, string name, List<Passanger> passangers) : base(id, name) {
            this.passangersCount = passangers.Count;
            this.passangers = passangers;
            passangers.ForEach(delegate (Passanger passanger)
            {
                passanger.baseTicketPrice = this.baseCost;
            });
        }

        public override double getFinalCost() {
            double result = 0;
            for (int i = 0; i < passangersCount; i++) {
                if (passangers[i].isDiscount)
                {
                    result += passangers[i].baseTicketPrice * 0.9;
                }
                else {
                    result += passangers[i].baseTicketPrice;
                }
            }
            return result;
        }
    }
}
