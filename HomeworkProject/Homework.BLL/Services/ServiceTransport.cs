﻿using Homework.BLL.MockDB;
using Homework.BLL.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework.BLL.Services
{
    public class ServiceTransport
    {
        private List<Transport> transports;
        public ServiceTransport()
        {
            Rides rides = new Rides();
            transports = rides.getRides();
        }
        public double calculateFinalCost(Transport transport)
        {
            return transport.getFinalCost();
        }

        public List<int> getRidesId(string type)
        {
            List<int> ids = new List<int>();
            if (type == "bus")
            {
                for (int i = 0; i < 20; i++)
                {
                    ids.Add(transports[i].getId());
                }
            }
            else if (type == "taxi")
            {
                for (int i = 20; i < 40; i++)
                {
                    ids.Add(transports[i].getId());
                }
            }
            else if (type == "train")
            {
                for (int i = 40; i < 60; i++)
                {
                    ids.Add(transports[i].getId());
                }
            }
            else if (type == "plane")
            {
                for (int i = 60; i < 80; i++)
                {
                    ids.Add(transports[i].getId());
                }
            }

            return ids;
        }

        public double getCostById(int id)
        {
            for (int i = 0; i < transports.Count; i++)
            {
                if (transports[i].getId() == id)
                {
                    return transports[i].getFinalCost();
                }
            }

            return 0;
        }
    }
}

