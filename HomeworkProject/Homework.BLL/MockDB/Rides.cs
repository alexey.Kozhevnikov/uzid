﻿using Homework.BLL.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Homework.BLL.MockDB
{
    public class Rides
    {
        private List<Transport> transports;
        public Rides() 
        {
            transports = new List<Transport>();
            int idCount = 0;
            Random rand = new Random();

            //Generate bus rides
            for (int i = 0; i < 20; i++) {
                List<Passanger> passangers = new List<Passanger>();
                for (int j = 0; j < 100; j++) {
                    Passanger passanger = new Passanger(idCount)
                    {
                        isDiscount = randomBool(),
                        baseTicketPrice = 25
                    };
                    passangers.Add(passanger);
                    idCount++;
                }
                Bus transport = new Bus(idCount, "Anonymous"+i, passangers);
                idCount++;
                transports.Add(transport);
            }

            //Generate taxi rides
            for (int i = 0; i < 20; i++)
            {
                List<Passanger> passangers = new List<Passanger>();
                for (int j = 0; j < 100; j++)
                {
                    Passanger passanger = new Passanger(idCount)
                    {
                        baseTicketPrice = 50,
                        rideDistance = rand.NextDouble() * 10000
                    };
                    passangers.Add(passanger);
                    idCount++;
                }
                Taxi transport = new Taxi(idCount, "Anonymous" + (i+20), passangers);
                idCount++;
                transports.Add(transport);
            }

            //Generate train rides
            for (int i = 0; i < 20; i++)
            {
                List<Passanger> passangers = new List<Passanger>();
                for (int j = 0; j < 100; j++)
                {
                    Passanger passanger = new Passanger(idCount)
                    {
                        isDiscount = randomBool(),
                        baseTicketPrice = rand.Next(5000, 15000),
                        age = rand.Next(10, 80)
                    };
                    passangers.Add(passanger);
                    idCount++;
                }
                Train transport = new Train(idCount, "Anonymous" + (i+40), passangers);
                idCount++;
                transports.Add(transport);
            }

            //Generate plane flights
            for (int i = 0; i < 20; i++)
            {
                List<Passanger> passangers = new List<Passanger>();
                for (int j = 0; j < 100; j++)
                {
                    Passanger passanger = new Passanger(idCount)
                    {
                        isDiscount = randomBool(),
                        baseTicketPrice = rand.Next(10000, 50000)
                    };
                    passangers.Add(passanger);
                    idCount++;
                }
                Plane transport = new Plane(idCount, "Anonymous" + (i+60), passangers);
                idCount++;
                transports.Add(transport);
            }
        }

        public List<Transport> getRides() 
        {
            return transports;
        }

        private bool randomBool() {
            Random rand = new Random();
            if (rand.Next(20) < 10)
            {
                return true;
            }
            else {
                return false;
            }

        }
    }
}
