import React, { useState } from 'react';
import { httpClient } from "../services/HttpClient";

const Form = () => {
    const [chosenType, setChosenType] = useState(false);
    const [chosenTransport, setChosenTransport] = useState();
    const [fieldNames, setFieldNames] = useState([]);

    const onChangeTransport = (event) => {
        event.preventDefault();
        const name = event.target.value;
        console.log(name);
        if (name === "#") {
            setChosenTransport("");
        }
        else {
            setChosenTransport(name);
        }
    };

    const Options = () => {
        var result = [];
        for (var key in fieldNames) {
            result.push(key);
        };
        return result;
    };

    const options = Options();


    const onChangeType = async (event) => {
        event.preventDefault();
        const typeName= event.target.value;
        if (typeName === "#") {
            setChosenType(false);
        }
        else {
            console.log(typeName);
            const fields = await httpClient.getFields(typeName);

            setFieldNames(fields);

            setChosenType(true);
        }  
    }

    const onSubmit = async (event) => {
        console.log(chosenTransport)
        const cost = await httpClient.getCost(chosenTransport)
        console.log(cost);
        document.getElementById("counter").value = cost;
    }

    return (
        <div className="h-full w-full">
            <form className="flex flex-col ml-4">
                <label>Choose ride type</label>
                <select onChange={onChangeType}>
                    <option key="#" value="#">Выберите шаблон</option>
                    <option id="transport" key="bus" value="bus">Bus</option>
                    <option id="transport" key="taxi" value="taxi">Taxi</option>
                    <option id="transport" key="train" value="train">Train</option>
                    <option id="transport" key="plane" value="plane">Plane</option>
                </select>
                {chosenType ?
                    <div className="fields-input-form">
                        <select onChange={onChangeTransport}>
                            <option key="#" value="#">Выберите ID</option>
                            {options.map((key) => (<option key={fieldNames[key]} value={fieldNames[key]}>{fieldNames[key]}</option>))}
                        </select>
                        <input type="button" onClick={onSubmit} value="Calculate cost" />
                        <input id="counter" type="number" disabled />
                    </div>
                        : 
                    <div className="fields-input-message px-4">
                        <div>Choose ride type</div>
                    </div>
                }
            </form>
        </div>
    );
}

 export default Form;
