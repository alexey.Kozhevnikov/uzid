import axios from "axios";

class HttpClient {
    constructor(transport) {
        this.transport = transport;
    }

    async getFields(type) {
        const result = await axios.post("api/getRidesId", { type });
        return result.data;
    }

    async getCost(id) {
        const result = await axios.post("api/getCost", { id });
        return result.data;
    }
}

export const httpClient = new HttpClient(axios);
