﻿using System;

namespace Homework
{
    public class Passanger
    {
        public int id { get; set; }
        public bool hasDiscount { get; set; }
        public int age { get; set; }
        public double rideDistance { get; set; }
        public double baseCost { get; set; }
    }
}
