﻿using System;
using System.Collections.Generic;

namespace Homework
{
    public class Transport
    {
        public int id { get; set; }
        public string driverName { get; set; }
        public string rideType { get; set; }
        public List<Passanger> passangers { get; set; }
    }
}
