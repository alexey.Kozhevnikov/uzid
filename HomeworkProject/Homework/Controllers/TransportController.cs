﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Homework.BLL.Services;
using Homework.BLL.Objects;
using Homework.Model;

namespace Homework.Controllers
{
    [ApiController]
    [Route("api")]
    public class TransportController : ControllerBase
    {
        [HttpPost("getCostRide")]
        public double GetFinalCost([FromBody] Transport transport)
        {
            double result = 0;
            ServiceTransport service = new ServiceTransport();
            List<Passanger> clients = transport.passangers;
            List<BLL.Objects.Passanger> passangers = new List<BLL.Objects.Passanger>();
            clients.ForEach(delegate (Passanger client) {
                passangers.Add(new BLL.Objects.Passanger(client.id, client.hasDiscount, client.rideDistance, client.age, client.baseCost));
            });
            switch (transport.rideType)
            {
                case "Bus":
                    BLL.Objects.Bus bus = new Bus(transport.id, transport.driverName, passangers);
                    result = service.calculateFinalCost(bus);
                    break;
                case "Taxi":
                    BLL.Objects.Taxi taxi = new Taxi(transport.id, transport.driverName, passangers);
                    result = service.calculateFinalCost(taxi);
                    break;
                case "Plane":
                    BLL.Objects.Plane plane = new Plane(transport.id, transport.driverName, passangers);
                    result = service.calculateFinalCost(plane);
                    break;
                case "Train":
                    BLL.Objects.Train train = new Train(transport.id, transport.driverName, passangers);
                    result = service.calculateFinalCost(train);
                    break;
            }

            return result;
        }

        [HttpPost("getRidesId")]
        public List<int> GetRidesId([FromBody] Model.Type type)
        {
            ServiceTransport service = new ServiceTransport();
            List<int> result = service.getRidesId(type.type);

            return result;
        }

        [HttpPost("getCost")]
        public double GetCost([FromBody] Id id)
        {
            int intId = Convert.ToInt32(id.id);
            ServiceTransport service = new ServiceTransport();
            double result = service.getCostById(intId);

            return result;
        }
    }
}
